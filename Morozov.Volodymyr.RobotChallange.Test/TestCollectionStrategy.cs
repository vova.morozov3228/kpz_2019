﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MorozovVolodymyr.RobotChallange;
using Robot.Common;

namespace Morozov.Volodymyr.RobotChallange.Test
{
    [TestClass]
    public class TestCollectionStrategy
    {
        short roundCount = 1;
        private readonly Map _map = new Map() { MaxPozition = new Position() { X = 99, Y = 99 }, MinPozition = new Position() { X = 0, Y = 0 } };
        public string Author => "Morozov Volodymyr";

        [TestMethod]
        public void TestCollectFromOneStation()
        {
            var stationPosition = new Position(10, 10);
            var robotPosition = new Position(10, 10);
            var robotToMoveIndex = 0;
            var owner = new Owner
            {
                Name = "Morozov Volodymyr",
            };

            var robots =
                new List<Robot.Common.Robot>()
                {
                    new Robot.Common.Robot() {Energy = 100, OwnerName = owner.Name, Position = robotPosition}
                };


            _map.Stations.Add(new EnergyStation() { Energy = 100, Position = stationPosition });

            var commandManager = new CommandManager(Author);
            commandManager.SetMapData(robots, robotToMoveIndex, _map, roundCount);

            Assert.AreEqual(new CollectEnergyCommand().ToString(), commandManager.CreateCommand().ToString());
        }
    }
}
