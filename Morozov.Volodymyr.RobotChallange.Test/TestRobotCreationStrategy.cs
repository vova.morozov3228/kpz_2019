﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Robot.Common;
using MorozovVolodymyr.RobotChallange;

namespace Morozov.Volodymyr.RobotChallange.Test
{
    [TestClass]
    public class TestRobotCreationStrategy
    {
        short roundCount = 1;
        private readonly Map _map = new Map() { MaxPozition = new Position() { X = 99, Y = 99 }, MinPozition = new Position() { X = 0, Y = 0 } };
        public string Author => "Morozov Volodymyr";

        [TestMethod]
        public void TestRobotCreation()
        {
            var stationPosition = new Position(10, 10);
            var robotPosition = new Position(9, 9);
            var robotToMoveIndex = 0;
            var owner = new Owner
            {
                Name = "Morozov Volodymyr",
            };

            var robots =
                new List<Robot.Common.Robot>() { new Robot.Common.Robot() { Energy = 1000, OwnerName = owner.Name, Position = robotPosition } };


            _map.Stations.Add(new EnergyStation() { Energy = 100, Position = stationPosition });

            var commandCreator = new CommandManager(Author);
            commandCreator.SetMapData(robots, robotToMoveIndex, _map, roundCount);
            Assert.AreEqual(new CreateNewRobotCommand().ToString(), commandCreator.CreateCommand().ToString());
        }

        [TestMethod]
        public void TestRobotLimitOfCreation()
        {
            var stationPosition = new Position(1, 3);
            var robotPosition = new Position(0, 0);
            var robotToMoveIndex = 0;
            var owner = new Owner
            {
                Name = "Morozov Volodymyr",
            };

            var robots = new List<Robot.Common.Robot>();

            for (int i = 0; i < 100; ++i)
            {
                robots.Add(new Robot.Common.Robot()
                {
                    Energy = 1000,
                    OwnerName = owner.Name,
                    Position = new Position(robotPosition.X + i, robotPosition.Y)
                });
            }

            _map.Stations.Add(new EnergyStation() { Energy = 100, Position = stationPosition });

            var commandCreator = new CommandManager(Author);
            commandCreator.SetMapData(robots, robotToMoveIndex, _map, roundCount);
            Assert.AreEqual(new MoveCommand().ToString(), commandCreator.CreateCommand().ToString());
        }

        [TestMethod]
        public void TestRoundLimitOfCreation()
        {
            short round = 47;
            var stationPosition = new Position(10, 10);
            var robotPosition = new Position(4, 4);
            var robotToMoveIndex = 0;
            var owner = new Owner
            {
                Name = "Morozov Volodymyr",
            };

            var robots =
                new List<Robot.Common.Robot>() { new Robot.Common.Robot() { Energy = 1000, OwnerName = owner.Name, Position = robotPosition } };


            _map.Stations.Add(new EnergyStation() { Energy = 100, Position = stationPosition });

            var commandCreator = new CommandManager(Author);
            commandCreator.SetMapData(robots, robotToMoveIndex, _map, round);
            Assert.AreEqual(new MoveCommand().ToString(), commandCreator.CreateCommand().ToString());
        }
    }
}

