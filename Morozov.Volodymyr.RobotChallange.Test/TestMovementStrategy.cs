﻿using System;
using System.Collections.Generic;
using MorozovVolodymyr.RobotChallange;
using Robot.Common;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Morozov.Volodymyr.RobotChallange.Test
{
    [TestClass]
    public class TestMovementStrategy
    {
        short roundCount = 1;
        private readonly Map _map = new Map() { MaxPozition = new Position() { X = 99, Y = 99 }, MinPozition = new Position() { X = 0, Y = 0 } };
        public string Author => "Morozov Volodymyr";

        [TestMethod]
        public void TestMoveToStation()
        {
            var stationPosition = new Position(10, 10);
            var robotPosition = new Position(5, 5);
            var finishPosition = new Position(8, 8);
            var robotToMoveIndex = 0;
            var owner = new Owner
            {
                Name = "Morozov Volodymyr",
            };

            var robots =
                new List<Robot.Common.Robot>() { new Robot.Common.Robot() { Energy = 100, OwnerName = owner.Name, Position = robotPosition } };

            _map.Stations.Add(new EnergyStation() { Energy = 100, Position = stationPosition });

            var commandManager = new CommandManager(Author);
            commandManager.SetMapData(robots, robotToMoveIndex, _map, roundCount);
            Assert.AreEqual(finishPosition, (commandManager.CreateCommand() as MoveCommand)?.NewPosition);
        }

        [TestMethod]
        public void TestMoveToNearestStation()
        {
            var firstStationPosition = new Position(9, 9);
            var secondStationPosition = new Position(1, 1);
            var finishPosition = new Position(3, 3);
            var robotPosition = new Position(4, 4);
            var robotToMoveIndex = 0;
            var owner = new Owner
            {
                Name = "Morozov Volodymyr",
            };

            var robots =
                new List<Robot.Common.Robot>() { new Robot.Common.Robot() { Energy = 100, OwnerName = owner.Name, Position = robotPosition } };

            _map.Stations.Add(new EnergyStation() { Energy = 100, Position = firstStationPosition });
            _map.Stations.Add(new EnergyStation() { Energy = 100, Position = secondStationPosition });

            var commandManager = new CommandManager(Author);
            commandManager.SetMapData(robots, robotToMoveIndex, _map, roundCount);
            Assert.AreEqual(finishPosition, (commandManager.CreateCommand() as MoveCommand)?.NewPosition);
        }

        [TestMethod]
        public void TestMoveOnlyToFreeStation()
        {
            var firstStationPosition = new Position(7, 7);
            var secondStationPosition = new Position(1, 1);
            var robotPosition = new Position(5, 5);
            var secondRobotPosition = new Position(7, 7);
            var finishPosition = new Position(3, 3);
            var robotToMoveIndex = 0;
            var owner = new Owner
            {
                Name = "Morozov Volodymyr",
            };

            var robots =
                new List<Robot.Common.Robot>() { new Robot.Common.Robot() { Energy = 100, OwnerName = owner.Name, Position = robotPosition } };
            robots.Add(new Robot.Common.Robot() { Energy = 100, OwnerName = owner.Name, Position = secondRobotPosition });

            _map.Stations.Add(new EnergyStation() { Energy = 100, Position = firstStationPosition });
            _map.Stations.Add(new EnergyStation() { Energy = 100, Position = secondStationPosition });

            var commandManager = new CommandManager(Author);
            commandManager.SetMapData(robots, 1, _map, roundCount);
            commandManager.CreateCommand();

            commandManager.SetMapData(robots, robotToMoveIndex, _map, roundCount);
            Assert.AreEqual(finishPosition, (commandManager.CreateCommand() as MoveCommand)?.NewPosition);
        }

        [TestMethod]
        public void TestMoveToTheBestPlace()
        {
            var firstStationPosition = new Position(8, 8);
            var secondStationPosition = new Position(8, 1);
            var thirdStationPosition = new Position(8, 2);
            var robotPosition = new Position(8, 6);
            var finishPosition = new Position(8, 3);
            var robotToMoveIndex = 0;
            var owner = new Owner
            {
                Name = "Morozov Volodymyr",
            };

            var robots =
                new List<Robot.Common.Robot>() { new Robot.Common.Robot() { Energy = 150, OwnerName = owner.Name, Position = robotPosition } };

            _map.Stations.Add(new EnergyStation() { Energy = 100, Position = firstStationPosition });
            _map.Stations.Add(new EnergyStation() { Energy = 100, Position = secondStationPosition });
            _map.Stations.Add(new EnergyStation() { Energy = 100, Position = thirdStationPosition });

            var commandManager = new CommandManager(Author);
            commandManager.SetMapData(robots, robotToMoveIndex, _map, roundCount);
            Assert.AreEqual(finishPosition, (commandManager.CreateCommand() as MoveCommand)?.NewPosition);
        }

        [TestMethod]
        public void TestMoveTheFastestWay()
        {
            var stationPosition = new Position(5, 5);
            var robotPosition = new Position(8, 20);
            var finishPosition = new Position(7, 13);
            var robotToMoveIndex = 0;
            var owner = new Owner
            {
                Name = "Morozov Volodymyr",
            };

            var robots =
                new List<Robot.Common.Robot>() { new Robot.Common.Robot() { Energy = 100, OwnerName = owner.Name, Position = robotPosition } };

            _map.Stations.Add(new EnergyStation() { Energy = 100, Position = stationPosition });

            var commandManager = new CommandManager(Author);
            commandManager.SetMapData(robots, robotToMoveIndex, _map, roundCount);
            Assert.AreEqual(finishPosition, (commandManager.CreateCommand() as MoveCommand)?.NewPosition);
        }

        //[TestMethod]
        //public void TestHardWayWithEnemys()
        //{
        //    var stationPosition = new Position(8, 8);
        //    var robotPosition = new Position(8, 18);
        //    var enemyRobotPosition = new Position(8, 10);
        //    var finishPosition = new Position(7, 14);
        //    var robotToMoveIndex = 0;
        //    var owner = new Owner
        //    {
        //        Name = "Morozov Volodymyr",
        //    };
        //    var ownerEnemy = new Owner
        //    {
        //        Name = "Enemy",
        //    };

        //    var robots =
        //        new List<Robot.Common.Robot>() { new Robot.Common.Robot() { Energy = 100, OwnerName = owner.Name, Position = robotPosition } };
        //    robots.Add(new Robot.Common.Robot() { Energy = 150, OwnerName = ownerEnemy.Name, Position = enemyRobotPosition });

        //    _map.Stations.Add(new EnergyStation() { Energy = 100, Position = stationPosition });

        //    var commandManager = new CommandManager(Author);
        //    commandManager.SetMapData(robots, robotToMoveIndex, _map, roundCount);
        //    Assert.AreEqual(finishPosition, (commandManager.CreateCommand() as MoveCommand)?.NewPosition);
        //}

        [TestMethod]
        public void TestChangePosition()
        {
            var firstStationPosition = new Position(5, 5);
            var secondStationPosition = new Position(8, 5);
            var thirdStationPosition = new Position(9, 5);
            var robotPosition = new Position(5, 5);
            var finishPosition = new Position(7, 5);
            var robotToMoveIndex = 0;
            var owner = new Owner
            {
                Name = "Morozov Volodymyr",
            };

            var robots =
                new List<Robot.Common.Robot>() { new Robot.Common.Robot() { Energy = 150, OwnerName = owner.Name, Position = robotPosition } };

            _map.Stations.Add(new EnergyStation() { Energy = 100, Position = firstStationPosition });
            _map.Stations.Add(new EnergyStation() { Energy = 100, Position = secondStationPosition });
            _map.Stations.Add(new EnergyStation() { Energy = 100, Position = thirdStationPosition });

            var commandManager = new CommandManager(Author);
            commandManager.SetMapData(robots, robotToMoveIndex, _map, roundCount);
            Assert.AreEqual(finishPosition, (commandManager.CreateCommand() as MoveCommand)?.NewPosition);
        }

        [TestMethod]
        public void TestMoveWhenTwoEqualDistances()
        {
            var firstStationPosition = new Position(0, 0);
            var secondStationPosition = new Position(8, 8);
            var robotPosition = new Position(4, 4);
            var finishPosition = new Position(6, 6);
            var robotToMoveIndex = 0;
            var owner = new Owner
            {
                Name = "Morozov Volodymyr",
            };

            var robots =
                new List<Robot.Common.Robot>() { new Robot.Common.Robot() { Energy = 150, OwnerName = owner.Name, Position = robotPosition }, };

            _map.Stations.Add(new EnergyStation() { Energy = 100, Position = firstStationPosition });
            _map.Stations.Add(new EnergyStation() { Energy = 100, Position = secondStationPosition });

            var commandManager = new CommandManager(Author);
            commandManager.SetMapData(robots, robotToMoveIndex, _map, roundCount);
            Assert.AreEqual(finishPosition, (commandManager.CreateCommand() as MoveCommand)?.NewPosition);
        }

        [TestMethod]
        public void TestTheBestPlacing2()
        {
            var firstStationPosition = new Position(11, 11);
            var secondStationPosition = new Position(11, 14);
            var thirdStationPosition = new Position(15, 15);
            var robotPosition = new Position(10, 10);
            var finishPosition = new Position(13, 13);
            var robotToMoveIndex = 0;
            var owner = new Owner
            {
                Name = "Morozov Volodymyr",
            };

            var robots =
                new List<Robot.Common.Robot>() { new Robot.Common.Robot() { Energy = 150, OwnerName = owner.Name, Position = robotPosition } };

            _map.Stations.Add(new EnergyStation() { Energy = 100, Position = firstStationPosition });
            _map.Stations.Add(new EnergyStation() { Energy = 100, Position = secondStationPosition });
            _map.Stations.Add(new EnergyStation() { Energy = 100, Position = thirdStationPosition });

            var commandManager = new CommandManager(Author);
            commandManager.SetMapData(robots, robotToMoveIndex, _map, roundCount);
            Assert.AreEqual(finishPosition, (commandManager.CreateCommand() as MoveCommand)?.NewPosition);
        }

        [TestMethod]
        public void TestEdgeOfTheMap()
        {
            var forbiddenStationPosition = new Position(105, 105);
            var secondStationPosition = new Position(88, 88);
            var robotPosition = new Position(98, 98);
            var finishPosition = new Position(90, 90);
            var robotToMoveIndex = 0;
            var owner = new Owner
            {
                Name = "Morozov Volodymyr",
            };

            var robots =
                new List<Robot.Common.Robot>() { new Robot.Common.Robot() { Energy = 150, OwnerName = owner.Name, Position = robotPosition } };

            _map.Stations.Add(new EnergyStation() { Energy = 100, Position = forbiddenStationPosition });
            _map.Stations.Add(new EnergyStation() { Energy = 100, Position = secondStationPosition });


            var commandManager = new CommandManager(Author);
            commandManager.SetMapData(robots, robotToMoveIndex, _map, roundCount);
            Assert.AreEqual(finishPosition, (commandManager.CreateCommand() as MoveCommand)?.NewPosition);
        }
    }
}
