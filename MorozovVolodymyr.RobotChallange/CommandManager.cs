﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using MorozovVolodymyr.RobotChallange.Strategies;
using Robot.Common;

namespace MorozovVolodymyr.RobotChallange
{
    public class CommandManager
    {
        private readonly MapData mapData;
        private CollectionStrategy collectionStrategy;
        private MovementStrategy movementStrategy;
        private RobotCreationStrategy creationStrategy;


        public CommandManager(string author)
        {
            mapData = new MapData(author);
            collectionStrategy = new CollectionStrategy();
            movementStrategy = new MovementStrategy();
            creationStrategy = new RobotCreationStrategy();
        }

        public RobotCommand CreateCommand()
        {
            collectionStrategy.MapData = mapData;
            movementStrategy.MapData = mapData;
            creationStrategy.MapData = mapData;


            if (creationStrategy.IsSituationGoodForCreation())
            {
                return creationStrategy.CreateRobotCommand();
            }
            if (movementStrategy.IsSituationGoodForMove())
            {//TODO Situation when we dont need for move but dont have destination
                try
                {
                    var command = movementStrategy.CreateMoveCommand();
                    return command;
                }
                catch (Exception e)
                {
                    Debug.WriteLine(e);
                }
            }
            if (collectionStrategy.IsSituationGoodForCollection())
            {
                return collectionStrategy.CreateCollectCommand();
            }
            //TODO CHECK situations
            throw new Exception("Something wrong with me!");
        }

        public void SetMapData(IList<Robot.Common.Robot> robots, int robotToMoveIndex, Map map, short roundCount)
        {
            mapData.SetData(robots, robotToMoveIndex, map, roundCount);
        }
    }
}
