﻿using System;
using System.Collections.Generic;
using System.Linq;
using Robot.Common;

namespace MorozovVolodymyr.RobotChallange
{
    public class MapData
    {
        public const short CollectionRange = 2;

        public short RoundCount { get; private set; }
        public IList<Robot.Common.Robot> Robots { get; private set; }
        public IList<Robot.Common.Robot> MyRobots { get; private set; }
        public Robot.Common.Robot CurrentRobot { get; private set; }
        public int RobotToMoveIndex { get; private set; }
        public Map Map { get; private set; }
        public int[,] CellEvaluation { get; private set; }
        public Dictionary<int, Position> Destination { get; set; }
        public string Author { get; set; }

        public MapData(string author)
        {
            Destination = new Dictionary<int, Position>();
            Author = author;
        }

        public void SetData(IList<Robot.Common.Robot> robots, int robotToMoveIndex, Map map, short roundCount)
        {
            Robots = robots;
            RobotToMoveIndex = robotToMoveIndex;
            Map = map;
            RoundCount = roundCount;
            MyRobots = Robots.Where(a => a.OwnerName == Author).ToList();
            CurrentRobot = Robots[RobotToMoveIndex];
            if (CellEvaluation == null)
            {
                CellEvaluation = new int[Map.MaxPozition.X+1, Map.MaxPozition.Y+1];
                MakeCellEvaluation();
            }
        }

        private void MakeCellEvaluation()
        {
            var tempPos = new Position(0, 0);

            for (int i = Map.MinPozition.X; i <= Map.MaxPozition.X; ++i)
            {
                for (int j = Map.MinPozition.Y; j <= Map.MaxPozition.Y; ++j)
                {
                    tempPos.X = i;
                    tempPos.Y = j;
                    CellEvaluation[i, j] = HowManyStationsCanCollect(tempPos);
                }
            }
        }

        public int HowManyStationsCanCollect(Position fromPosition)
        {
            return Map.Stations.Count(station => (Math.Abs(station.Position.X - fromPosition.X) <= CollectionRange)
                && (Math.Abs(station.Position.Y - fromPosition.Y) <= CollectionRange));
        }

        public List<EnergyStation> GetStationsCanCollect(Position fromPosition)
        {
            return Map.Stations.Where(station => (Math.Abs(station.Position.X - fromPosition.X) <= CollectionRange)
                && (Math.Abs(station.Position.Y - fromPosition.Y) <= CollectionRange)).ToList();
        }

        public int HowManyMyRobotsNearby(Position fromPosition)
        {
            return MyRobots.Count(robot => (Math.Abs(robot.Position.X - fromPosition.X) <= CollectionRange)
                && (Math.Abs(robot.Position.Y - fromPosition.Y) <= CollectionRange));
        }

        public int HowManyEnemyRobotsNearby(Position fromPosition)
        {
            return Robots.Count(robot => (Math.Abs(robot.Position.X - fromPosition.X) <= CollectionRange)
                && (Math.Abs(robot.Position.Y - fromPosition.Y) <= CollectionRange));
        }

        public bool IsRobotNearby(Position fromPosition)
        {
            var currentPosition = new Position(-1, -1);
            for (int i = -CollectionRange; i <= CollectionRange; ++i)
            {
                for (int j = -CollectionRange; j <= CollectionRange; ++j)
                {
                    currentPosition.X = fromPosition.X + i;
                    currentPosition.Y = fromPosition.Y + j;
                    currentPosition = UpdatePosition(currentPosition);
                    if (Destination.ContainsValue(currentPosition))
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        public Position UpdatePosition(Position old)
        {
            var newPosition = new Position(old.X, old.Y);

            if (newPosition.X < 0)
            {
                newPosition.X *= -1;
            }
            if (newPosition.X > 99)
            {
                newPosition.X = 99;
            }
            if (newPosition.Y < 0)
            {
                newPosition.Y *= -1;
            }
            if (newPosition.Y > 99)
            {
                newPosition.Y = 99;
            }

            return newPosition;
        }
    }
}
