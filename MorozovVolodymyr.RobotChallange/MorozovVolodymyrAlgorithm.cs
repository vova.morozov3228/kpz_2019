﻿using Robot.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MorozovVolodymyr.RobotChallange
{
    public class MorozovVolodymyrAlgorithm : IRobotAlgorithm
    {
        private short RoundCount;
        private readonly CommandManager commandManager;

        public MorozovVolodymyrAlgorithm()
        {
            RoundCount = 0;
            Logger.OnLogRound += (sender, e) => ++RoundCount;
            commandManager = new CommandManager(Author);
        }

        public string Author => "Morozov Volodymyr";

        public string Description => string.Concat(
                        "This is the algorithm by Vouchik Morozov, the most clever and stupid programmer in his room.",
                        "If you found algorithm which is better - it's cheating, unreal, delete it.");

        public RobotCommand DoStep(IList<Robot.Common.Robot> robots, int robotToMoveIndex, Map map)
        {
            commandManager.SetMapData(robots, robotToMoveIndex, map, RoundCount);
            return commandManager.CreateCommand();
        }
    }
}
