﻿using System;
using System.Linq;
using Robot.Common;

namespace MorozovVolodymyr.RobotChallange.Strategies
{
    public class MovementStrategy
    {
        private const double MaxEnergyForMove = 0.6;

        public MapData MapData { private get; set; }

        public bool IsSituationGoodForMove()
        {
            return !CurrentRobotHasDestination() || !IsCurrentRobotOnDestination() ||
                   CanFindBetterDestination() != null;
        }
        //TODO check null variant
        public RobotCommand CreateMoveCommand()
        {
            Position resultPosition;

            if (!MapData.Destination.ContainsKey(MapData.RobotToMoveIndex))
            {
                var destination = FindDestination();

                MapData.Destination.Add(MapData.RobotToMoveIndex, destination);
                resultPosition = NextPosition(destination);

                if (resultPosition == MapData.CurrentRobot.Position)
                {
                    throw new Exception("You are in a great position!");
                }
            }
            else
            {
                resultPosition = NextPosition(MapData.Destination[MapData.RobotToMoveIndex]);
            }

            if (resultPosition != null)
            {
                return new MoveCommand() { NewPosition = resultPosition };
            }

            throw new Exception("I do not know where to go!");
        }

        public bool CurrentRobotHasDestination()
        {
            return MapData.Destination.ContainsKey(MapData.RobotToMoveIndex);
        }

        public bool IsCurrentRobotOnDestination()
        {
            return MapData.Destination[MapData.RobotToMoveIndex] == MapData.CurrentRobot.Position;
        }

        //TODO Create method
        public Position CanFindBetterDestination()
        {
            return null;
        }

        public Position FindDestination()
        {
            const short maxStepsCount = 100;
            var searchRadius = (int)Math.Sqrt(MapData.CurrentRobot.Energy);

            try
            {
                for (int i = 1; i < maxStepsCount; ++i)
                {
                    searchRadius *= i;
                    var result = SearchTheBestPositionInArea(MapData.CurrentRobot.Position, searchRadius);
                    if (result != null)
                    {
                        return result;
                    }
                }
                return null;
            }
            catch (Exception)
            {
                return null;
            }
        }

        //TODO change calculate for new rules
        public static int FindEnergyForMovement(Position a, Position b)
        {
            return (int)(Math.Pow(a.X - b.X, 2) + Math.Pow(a.Y - b.Y, 2));
        }

        private bool IsCellAndAreaFree(Position cellPosition)
        {
            return !MapData.IsRobotNearby(cellPosition);
        }

        private bool CurrentRobotCanMoveToPosition(Position finishPosition)
        {
            return FindEnergyForMovement(MapData.CurrentRobot.Position, finishPosition) <=
                   MapData.CurrentRobot.Energy;
        }

        private Position SearchTheBestPositionInArea(Position center, int radius)
        {
            var maxMark = 0;
            var cellPosition = new Position(-1, -1);
            var currentPosition = new Position(-1, -1);

            for (int i = -radius; i <= radius; ++i)
            {
                for (int j = -radius; j <= radius; ++j)
                {
                    currentPosition.X = center.X + i;
                    currentPosition.Y = center.Y + j;
                    currentPosition = MapData.UpdatePosition(currentPosition);

                    if (MapData.CellEvaluation[currentPosition.X, currentPosition.Y] > maxMark &&
                        IsCellAndAreaFree(currentPosition))
                    {
                        maxMark = MapData.CellEvaluation[currentPosition.X, currentPosition.Y];
                        cellPosition.X = currentPosition.X;
                        cellPosition.Y = currentPosition.Y;
                    }
                    else if (maxMark >= 1 && MapData.CellEvaluation[currentPosition.X, currentPosition.Y] == maxMark &&
                            FindEnergyForMovement(MapData.CurrentRobot.Position, cellPosition) >
                            FindEnergyForMovement(MapData.CurrentRobot.Position,
                                currentPosition) &&
                            IsCellAndAreaFree(currentPosition))
                    {
                        {
                            maxMark = MapData.CellEvaluation[currentPosition.X, currentPosition.Y];
                            cellPosition.X = currentPosition.X;
                            cellPosition.Y = currentPosition.Y;
                        }
                    }

                }
            }

            if (cellPosition.X == -1)
            {
                return null;
            }

            if (MapData.CellEvaluation[MapData.CurrentRobot.Position.X, MapData.CurrentRobot.Position.Y] == maxMark &&
                IsCellAndAreaFree(MapData.CurrentRobot.Position))
            {
                cellPosition = MapData.CurrentRobot.Position;
            }

            return cellPosition;
        }

        private int HowManySteps(out Position recommendedPosition, Position finishPosition)
        {
            Position temporaryNextPosition = new Position(finishPosition.X, finishPosition.Y); ;
            recommendedPosition = new Position(temporaryNextPosition.X, temporaryNextPosition.Y);

            for (var i = 1; i < byte.MaxValue; ++i)
            {
                temporaryNextPosition.X = (int)(MapData.CurrentRobot.Position.X + (temporaryNextPosition.X - MapData.CurrentRobot.Position.X) / (float)i);
                temporaryNextPosition.Y = (int)(MapData.CurrentRobot.Position.Y + (temporaryNextPosition.Y - MapData.CurrentRobot.Position.Y) / (float)i);

                if (FindEnergyForMovement(MapData.CurrentRobot.Position, temporaryNextPosition) * i <= MapData.CurrentRobot.Energy)
                {
                    recommendedPosition.X = temporaryNextPosition.X;
                    recommendedPosition.Y = temporaryNextPosition.Y;
                    return i;
                }
                temporaryNextPosition.X = finishPosition.X;
                temporaryNextPosition.Y = finishPosition.Y;
            }

            return 0;
        }
       
        private Position FindNewPosition(Position oldPosition)
        {
            var newPosition = new Position() { X = oldPosition.X, Y = oldPosition.Y };

            if (oldPosition.X > 0)
                newPosition.X = oldPosition.X - 1;
            else
                newPosition.X = oldPosition.X + 1;

            if (MapData.MyRobots.All(x => x.Position != newPosition))
            {
                return newPosition;
            }

            if (oldPosition.Y > 0)
                newPosition.Y = oldPosition.Y - 1;
            else
                newPosition.Y = oldPosition.Y + 1;

            if (MapData.MyRobots.All(x => x.Position != newPosition))
            {
                return newPosition;
            }

            if (oldPosition.Y < 100)
                newPosition.Y = oldPosition.Y + 1;
            else
                newPosition.Y = oldPosition.Y - 1;

            return newPosition;
        }

        private Position NextPosition(Position finishPosition)
        {
            if (HowManySteps(out var recommendedPosition, finishPosition) != 0)
            {
                foreach (var robot in MapData.Robots)
                {
                    if (robot != MapData.CurrentRobot && recommendedPosition == robot.Position)
                    {
                        recommendedPosition = FindNewPosition(recommendedPosition);
                        return recommendedPosition;
                    }
                }

                return recommendedPosition;
            }

            return null;
        }
    }
}
