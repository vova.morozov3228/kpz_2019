﻿using Robot.Common;

namespace MorozovVolodymyr.RobotChallange.Strategies
{
    public class RobotCreationStrategy
    {
        public MapData MapData { private get; set; }

        public bool IsSituationGoodForCreation()
        {
            var maxRoundForCreation = 35;
            var maxRobotCount = 100;
            int minEnergyForCreate = 250;
            //TODO add creation analys round
            if (MapData.RoundCount < 8)
            {
                minEnergyForCreate = 200;
            }
            if (MapData.RoundCount > 20)
            {
                minEnergyForCreate = 150 + MapData.RoundCount * 5;
            }
            return MapData.CurrentRobot.Energy > minEnergyForCreate && MapData.RoundCount < maxRoundForCreation &&
                   MapData.MyRobots.Count < maxRobotCount;
        }

        public RobotCommand CreateRobotCommand()
        {
            return new CreateNewRobotCommand() { NewRobotEnergy = 100 };
        }
    }
}
