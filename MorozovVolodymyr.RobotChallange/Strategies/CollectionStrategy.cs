﻿using Robot.Common;
using System.Collections.Generic;

namespace MorozovVolodymyr.RobotChallange.Strategies
{
    public class CollectionStrategy
    {
        public MapData MapData { private get; set; }

        public bool IsSituationGoodForCollection()
        {
            return MapData.HowManyStationsCanCollect(MapData.CurrentRobot.Position) > 0;
        }

        public bool WillCollect(Position position)
        {
            List<EnergyStation> stations = MapData.GetStationsCanCollect(position);
            foreach (var station in stations) {

            }
            return true;
        }

        public RobotCommand CreateCollectCommand()
        {
            return new CollectEnergyCommand();
        }
    }
}
